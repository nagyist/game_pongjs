/**
* Author: Iker Jamardo Zugaza
* Company: Ludei (www.ludei.com)
* Disclaimer: 
    Please, note that the purpose of this software is merely educational. Plenty
    of error checking should be added to make it for professional use.
* License: LGPL v3
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
* ======================
* Pong.Timer
* ======================
* This type represents a simple timer to calculate the accumulated and elapsed time intervals between updates.
*/
Pong.Timer = function() {
    this.reset();
    return this;
};

Pong.Timer.prototype = {
    currentTimeInMillis : 0,
    lastTimeInMillis : 0,
    elapsedTimeInMillis : 0,
    elapsedTimeInSeconds : 0,
    accumTimeInMillis : 0,

    /**
    * Resets the timer (retrieves the current time, accumulated and elapsed times are set to 0, etc).
    */
    reset : function() {
        this.currentTimeInMillis = this.lastTimeInMillis = new Date().getTime();
        this.accumTimeInMillis = this.elapsedTimeInMillis = this.elapsedTimeInSeconds = 0;
    },

    /**
    * Updates the timer by calculating the accumulated time since the first time this timer was reset.
    * It also calculated the elapsed time beween update calls. Call this function in every frame to calculate the
    * time elapse between them.
    */
    update : function() {
        this.currentTimeInMillis = new Date().getTime();
        this.elapsedTimeInMillis = this.currentTimeInMillis - this.lastTimeInMillis;
        this.elapsedTimeInSeconds = this.elapsedTimeInMillis / 1000.0;
        this.lastTimeInMillis = this.currentTimeInMillis;
        this.accumTimeInMillis += this.elapsedTimeInMillis;
    }
};
